# Start args #
1. 0-9 -> Chain number
2. (optional) name of input device
3. (optional) name of output device

# Adding effect chain #
* To switch-case selecting chain in bz_callback() add:
```c
    case yourChainNumber:
        outFrame = yourChainFunction(&ch);
        break;
```

* Add declaration of yourChainFunction to bzMulti.h
* Create .c file with yourChainFunction

# TO DO: #
* Allow effect with periodic time functions to use different waveshapes
* Rework chain.c to init effect structure and then call effect(params) functions
  instead of block of code
* Rework arguments to main()
* Pass number of In/Out devices as args instead of hardcoding