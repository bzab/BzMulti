#ifndef SRC_BZMULTI_H_
#define SRC_BZMULTI_H_

#define SAMPLE_RATE         (44100)
#define PA_SAMPLE_TYPE      paFloat32
#define FRAMES_PER_BUFFER   (64)

#define D_MEMLEN 131072
#define FL_MEMLEN 16384

typedef float sample;

typedef struct {
	int len;
	sample sa[2];
} frame;

typedef struct {
	int inputsNum;
	int outputsNum;
	int chainNum;
} cbParams;

typedef struct {
	frame fr;
} chainParams;

typedef struct {
	float time; // Seconds
	int timeInSamples;
	int sampleNum;
} periodic;

///////////////////////////////////////
// EFFECT PARAMS:                    //
///////////////////////////////////////

typedef struct {
	int deld;
	float depth;
	sample dmem[2][D_MEMLEN];
	periodic sharedTimeParams;
	float vol;
} delayParams;

typedef struct {
	int sinArgCnt;
	float depth;
	float freq; //Hz
	sample fmem[2][FL_MEMLEN];
	periodic sharedTimeParams;
	float vol;
} flangerParams;

typedef struct {
	float depth;
	float gain;
	float vol;
} fuzzParams;

typedef struct {
	float depth;
	periodic sharedTimeParams;
	float vol;
} panParams;

typedef struct {
	float depth;
	periodic sharedTimeParams;
	float vol;
} tremoloParams;

///////////////////////////////////////
// EFFECTS:                          //
///////////////////////////////////////

frame delay(frame *fr, void *delayParams);
frame flanger(frame *fr, void *flangerParams);
frame fuzz(frame *fr, void *fuzzParams);
frame pan(frame *fr, void *stereoPulseParams);
frame tremolo(frame *fr, void *tremoloParams);

///////////////////////////////////////
// CHAINS:                           //
///////////////////////////////////////

frame chain0(void *chainParams);
frame chain1(void *chainParams);
frame chain2(void *chainParams);
frame chain3(void *chainParams);
frame chain4(void *chainParams);
frame chain5(void *chainParams);
frame chain6(void *chainParams);
frame chain7(void *chainParams);
frame chain8(void *chainParams);
frame chain9(void *chainParams);

#endif /* SRC_BZMULTI_H_ */
