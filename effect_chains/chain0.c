#include "../bzMulti.h"

// Clean chain

frame chain0(void *userParams)
{
	chainParams * params;
	params = (chainParams *) userParams;

	//Input volume
	float inVol = 1.0;
	for(int i=0; i<params->fr.len; i++) params->fr.sa[i] *= inVol;

	return params->fr;
}
