#include "../bzMulti.h"

frame chain1(void *userParams)
{
	chainParams * params;
	params = (chainParams *) userParams;

	//Input volume
	float inVol = 0.80;
	for(int i=0; i<params->fr.len; i++) params->fr.sa[i] *= inVol;

	//////////////////
	// Effect chain //
	//////////////////

	//Crunch
	static fuzzParams pDist[] = {
			{ .depth=1.0, .gain=1.05, .vol=1.0 },
			{ .depth=0.8, .gain=74.95, .vol=0.8 },
			{ .depth=0.5, .gain=6.75, .vol=0.65 },
	};
	for(int i=0; i<sizeof(pDist)/sizeof(fuzzParams); i++) params->fr = fuzz(&params->fr,&pDist[i]);

	// Delay                                           init
	static delayParams p0 = { .depth=0.07, .dmem[0][0]=-9999.0, .sharedTimeParams.sampleNum=0,
			.sharedTimeParams.time=0.95, .vol=0.9};
	p0.sharedTimeParams.timeInSamples = SAMPLE_RATE*p0.sharedTimeParams.time;
	params->fr = delay(&params->fr, &p0); // Call
	p0.sharedTimeParams.sampleNum++;
	p0.deld++;
	if(p0.sharedTimeParams.sampleNum >= D_MEMLEN) p0.sharedTimeParams.sampleNum=0;
	if(p0.deld >= D_MEMLEN) p0.deld=0;

	// Tremolo
	static tremoloParams p2 = { .depth=0.5, .sharedTimeParams.time = 0.01, .sharedTimeParams.sampleNum=0, .vol=1.0 };
	p2.sharedTimeParams.timeInSamples = SAMPLE_RATE*p2.sharedTimeParams.time;
	params->fr = tremolo(&params->fr,&p2); // Call
	p2.sharedTimeParams.sampleNum++;
	if(p2.sharedTimeParams.sampleNum>=p2.sharedTimeParams.timeInSamples) p2.sharedTimeParams.sampleNum=0;

	//Flanger
	static flangerParams p1 = { .depth=0.4, .freq=0.5, .fmem[0][0]=-9999.0, .sinArgCnt=0,
			.sharedTimeParams.sampleNum=0, .sharedTimeParams.time=0.008, .vol=0.9 };
	p1.sharedTimeParams.timeInSamples = SAMPLE_RATE*p1.sharedTimeParams.time;
	params->fr = flanger(&params->fr, &p1); // Call

	p1.sinArgCnt++;
	if(p1.sinArgCnt >= SAMPLE_RATE) p1.sinArgCnt=0;
	p1.sharedTimeParams.sampleNum++;
	if(p1.sharedTimeParams.sampleNum >= FL_MEMLEN) p1.sharedTimeParams.sampleNum=0;

	// Panning
	static panParams p3 = { .depth=0.25, .sharedTimeParams.sampleNum=0, .sharedTimeParams.time=2.0, .vol=1.0 };
	p3.sharedTimeParams.timeInSamples = SAMPLE_RATE*p3.sharedTimeParams.time;
	params->fr = pan(&params->fr,&p3); // Call
	p3.sharedTimeParams.sampleNum++;
	if(p3.sharedTimeParams.sampleNum>=p3.sharedTimeParams.timeInSamples) p3.sharedTimeParams.sampleNum=0;

	//////////////////
	// End of chain //
    //////////////////

	return params->fr;
}
