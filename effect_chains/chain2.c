#include "../bzMulti.h"

frame chain2(void *userParams)
{
	chainParams * params;
	params = (chainParams *) userParams;

	//Input volume
	float inVol = 0.90;
	for(int i=0; i<params->fr.len; i++) params->fr.sa[i] *= inVol;

	//////////////////
	// Effect chain //
	//////////////////

	//Flanger
	static flangerParams p1 = { .depth=0.2, .freq=0.5, .fmem[0][0]=-9999.0, .sinArgCnt=0,
			.sharedTimeParams.sampleNum=0, .sharedTimeParams.time=0.008, .vol=0.9 };
	p1.sharedTimeParams.timeInSamples = SAMPLE_RATE*p1.sharedTimeParams.time;
	params->fr = flanger(&params->fr, &p1); // Call

	p1.sinArgCnt++;
	if(p1.sinArgCnt >= SAMPLE_RATE) p1.sinArgCnt=0;
	p1.sharedTimeParams.sampleNum++;
	if(p1.sharedTimeParams.sampleNum >= FL_MEMLEN) p1.sharedTimeParams.sampleNum=0;

	// Tremolo
	static tremoloParams p2 = { .depth=0.7, .sharedTimeParams.time = 0.1, .sharedTimeParams.sampleNum=0, .vol=1.0 };
	p2.sharedTimeParams.timeInSamples = SAMPLE_RATE*p2.sharedTimeParams.time;
	params->fr = tremolo(&params->fr,&p2); // Call
	p2.sharedTimeParams.sampleNum++;
	if(p2.sharedTimeParams.sampleNum>=p2.sharedTimeParams.timeInSamples) p2.sharedTimeParams.sampleNum=0;

	// Panning
	static panParams p3 = { .depth=0.25, .sharedTimeParams.sampleNum=0, .sharedTimeParams.time=2.0, .vol=1.0 };
	p3.sharedTimeParams.timeInSamples = SAMPLE_RATE*p3.sharedTimeParams.time;
	params->fr = pan(&params->fr,&p3); // Call
	p3.sharedTimeParams.sampleNum++;
	if(p3.sharedTimeParams.sampleNum>=p3.sharedTimeParams.timeInSamples) p3.sharedTimeParams.sampleNum=0;

	//////////////////
	// End of chain //
    //////////////////

	return params->fr;
}
