#include "../bzMulti.h"

frame chain4(void *userParams)
{
	chainParams * params;
	params = (chainParams *) userParams;

	//Input volume
	float inVol = 1.0;
	for(int i=0; i<params->fr.len; i++) params->fr.sa[i] *= inVol;

	//////////////////
	// Effect chain //
	//////////////////

	//Flanger
	static flangerParams p1 = { .depth=0.6, .freq=0.33, .fmem[0][0]=-9999.0, .sinArgCnt=0,
			.sharedTimeParams.sampleNum=0, .sharedTimeParams.time=0.008, .vol=1.0 };
	p1.sharedTimeParams.timeInSamples = SAMPLE_RATE*p1.sharedTimeParams.time;
	params->fr = flanger(&params->fr, &p1); // Call

	p1.sinArgCnt++;
	if(p1.sinArgCnt >= SAMPLE_RATE) p1.sinArgCnt=0;
	p1.sharedTimeParams.sampleNum++;
	if(p1.sharedTimeParams.sampleNum >= FL_MEMLEN) p1.sharedTimeParams.sampleNum=0;

	// Delay                                           init
	static delayParams p0 = { .depth=0.07, .dmem[0][0]=-9999.0, .sharedTimeParams.sampleNum=0,
			.sharedTimeParams.time=0.7, .vol=1.0};
	p0.sharedTimeParams.timeInSamples = SAMPLE_RATE*p0.sharedTimeParams.time;
	params->fr = delay(&params->fr, &p0); // Call
	p0.sharedTimeParams.sampleNum++;
	p0.deld++;
	if(p0.sharedTimeParams.sampleNum >= D_MEMLEN) p0.sharedTimeParams.sampleNum=0;
	if(p0.deld >= D_MEMLEN) p0.deld=0;

	//////////////////
	// End of chain //
    //////////////////

	return params->fr;
}
