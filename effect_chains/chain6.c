#include "../bzMulti.h"

frame chain6(void *userParams)
{
	chainParams * params;
	params = (chainParams *) userParams;

	//Input volume
	float inVol = 0.95;
	for(int i=0; i<params->fr.len; i++) params->fr.sa[i] *= inVol;

	//////////////////
	// Effect chain //
	//////////////////

	//Flanger
	static flangerParams p1 = { .depth=0.6, .freq=0.7, .fmem[0][0]=-9999.0, .sinArgCnt=0,
			.sharedTimeParams.sampleNum=0, .sharedTimeParams.time=0.008, .vol=0.9 };
	p1.sharedTimeParams.timeInSamples = SAMPLE_RATE*p1.sharedTimeParams.time;
	params->fr = flanger(&params->fr, &p1); // Call

	p1.sinArgCnt++;
	if(p1.sinArgCnt >= SAMPLE_RATE) p1.sinArgCnt=0;
	p1.sharedTimeParams.sampleNum++;
	if(p1.sharedTimeParams.sampleNum >= FL_MEMLEN) p1.sharedTimeParams.sampleNum=0;

	//////////////////
	// End of chain //
    //////////////////

	return params->fr;
}
