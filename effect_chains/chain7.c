#include "../bzMulti.h"

frame chain7(void *userParams)
{
	chainParams * params;
	params = (chainParams *) userParams;

	//Input volume
	float inVol = 0.90;
	for(int i=0; i<params->fr.len; i++) params->fr.sa[i] *= inVol;

	//////////////////
	// Effect chain //
	//////////////////

	// Tremolo
	static tremoloParams p2 = { .depth=0.7, .sharedTimeParams.time = 0.2, .sharedTimeParams.sampleNum=0, .vol=1.0 };
	p2.sharedTimeParams.timeInSamples = SAMPLE_RATE*p2.sharedTimeParams.time;
	params->fr = tremolo(&params->fr,&p2); // Call
	p2.sharedTimeParams.sampleNum++;
	if(p2.sharedTimeParams.sampleNum>=p2.sharedTimeParams.timeInSamples) p2.sharedTimeParams.sampleNum=0;

	//////////////////
	// End of chain //
    //////////////////

	return params->fr;
}
