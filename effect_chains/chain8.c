#include "../bzMulti.h"

frame chain8(void *userParams)
{
	chainParams * params;
	params = (chainParams *) userParams;

	//Input volume
	float inVol = 0.90;
	for(int i=0; i<params->fr.len; i++) params->fr.sa[i] *= inVol;

	//////////////////
	// Effect chain //
	//////////////////

	// Panning
	static panParams p3 = { .depth=1.0, .sharedTimeParams.sampleNum=0, .sharedTimeParams.time=1.0, .vol=1.0 };
	p3.sharedTimeParams.timeInSamples = SAMPLE_RATE*p3.sharedTimeParams.time;
	params->fr = pan(&params->fr,&p3); // Call
	p3.sharedTimeParams.sampleNum++;
	if(p3.sharedTimeParams.sampleNum>=p3.sharedTimeParams.timeInSamples) p3.sharedTimeParams.sampleNum=0;

	//////////////////
	// End of chain //
    //////////////////

	return params->fr;
}
