#include "../bzMulti.h"

frame chain9(void *userParams)
{
	chainParams * params;
	params = (chainParams *) userParams;

	//Input volume
	float inVol = 0.80;
	for(int i=0; i<params->fr.len; i++) params->fr.sa[i] *= inVol;

	//////////////////
	// Effect chain //
	//////////////////

	//Crunch
	static fuzzParams pDist[] = {
			{ .depth=1.0, .gain=1.05, .vol=1.0 },
			{ .depth=0.8, .gain=74.95, .vol=0.8 },
			{ .depth=0.5, .gain=6.75, .vol=0.65 },
	};
	for(int i=0; i<sizeof(pDist)/sizeof(fuzzParams); i++) params->fr = fuzz(&params->fr,&pDist[i]);

	//////////////////
	// End of chain //
    //////////////////

	return params->fr;
}
