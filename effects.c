#include <math.h>

#include "bzMulti.h"

// Effects:
frame delay(frame *fr, void *argParams)
{
	delayParams * params;
	params = (delayParams *) argParams;

	// Delaying line init
	if(params->dmem[0][0]==-9999.0)
	{
		for(int i=0; i<D_MEMLEN; i++)
		{
			for(int j=0; j<fr->len;j++) params->dmem[j][i]=0.0;
		}
		params->deld = D_MEMLEN - params->sharedTimeParams.timeInSamples;
	}

    for(int i=0; i<fr->len; i++)
    {
		fr->sa[i] += params->depth*params->dmem[i][params->deld];
		params->dmem[i][ params->sharedTimeParams.sampleNum ] = fr->sa[i];
		fr->sa[i] *= params->vol;
    }

	return *fr;
}

frame flanger(frame *fr, void *argParams)
{
	flangerParams * params;
	params = (flangerParams *) argParams;
	const double pi2 = 2.0 * M_PI;

	// Delaying line init
	if(params->fmem[0][0]==-9999.0)
	{
		for(int i=0; i<FL_MEMLEN; i++)
		{
			for(int j=0; j<fr->len;j++) params->fmem[j][i]=0.0;
		}
	}

	double sinArg = params->freq * pi2 * params->sinArgCnt / SAMPLE_RATE;
	int deld = params->sharedTimeParams.sampleNum - params->sharedTimeParams.timeInSamples*(1+sin(sinArg));
	if(deld >= FL_MEMLEN) deld %= FL_MEMLEN;
	else if(deld < 0) deld+=FL_MEMLEN;

    for(int i=0; i<fr->len; i++)
    {
		fr->sa[i] *= 1.0-params->depth;
    	fr->sa[i] += params->depth*params->fmem[i][deld];
		params->fmem[i][params->sharedTimeParams.sampleNum] = fr->sa[i];
		fr->sa[i] *= params->vol;
    }

	return *fr;
}

frame fuzz(frame *fr, void *argParams)
{
	fuzzParams * params;
	params = (fuzzParams *) argParams;

	sample input[2] = { fr->sa[0], fr->sa[1] };

	for(int i=0; i<fr->len; i++)
	{
		if(fr->sa[i]>0) fr->sa[i] = 1-exp(-fr->sa[i]*params->gain);
		else fr->sa[i] = exp(fr->sa[i]*params->gain)-1;
		fr->sa[i] *= params->depth;
		fr->sa[i] += (1.0-params->depth)*input[i];
		fr->sa[i] *= params->vol;
	}

	return *fr;
}

frame pan(frame *fr, void *argParams)
{
	panParams * params;
	params = (panParams *) argParams;
    const double pi2 = 2.0 * M_PI;

    sample input[2] = {fr->sa[0],fr->sa[1]};

    if(fr->len==1)
    {
    	input[1]=input[0]; // if input is mono
    	fr->len=2;
    }

	fr->sa[0] = input[0]*(1.0+params->depth*sin( pi2*params->sharedTimeParams.sampleNum/params->sharedTimeParams.timeInSamples )) * params->vol; // Left channel
	fr->sa[1] = input[1]*(1.0-params->depth*sin( pi2*params->sharedTimeParams.sampleNum/params->sharedTimeParams.timeInSamples )) * params->vol; // Right channel

	return *fr;
}

frame tremolo(frame *fr, void *argParams)
{
	tremoloParams * params;
	params = (tremoloParams *) argParams;

	sample input[2] = { fr->sa[0], fr->sa[1] };

	for(int i=0; i < fr->len; i++)
	{
		if(2*params->sharedTimeParams.sampleNum < params->sharedTimeParams.timeInSamples)
		{
			fr->sa[i] = input[i] * 2.0 * params->sharedTimeParams.sampleNum / params->sharedTimeParams.timeInSamples;
		} else {
			fr->sa[i] = input[i] * (2.0 - 2.0 * params->sharedTimeParams.sampleNum / params->sharedTimeParams.timeInSamples);
		}
		fr->sa[i] *= params->depth;
		fr->sa[i] += (1.0-params->depth) * input[i];
		fr->sa[i] *= params->vol;
	}

	return *fr;
}
