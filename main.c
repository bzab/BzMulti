#include <stdio.h>
#include <string.h>
#include <math.h>
#include <portaudio.h>

#include "bzMulti.h"

int gNumNoInputs = 0;

int Error(PaError err)
{
    Pa_Terminate();
    fprintf(stderr, "An error occured while using the portaudio stream\n");
    fprintf(stderr, "Error number: %d\n", err);
    fprintf(stderr, "Error message: %s\n", Pa_GetErrorText(err));
    return -1;
}

static int bz_callback(const void *inputBuffer, void *outputBuffer,
                         unsigned long framesPerBuffer,
                         const PaStreamCallbackTimeInfo* timeInfo,
                         PaStreamCallbackFlags statusFlags,
                         void *userData)
{
	cbParams *data = (cbParams*)userData;
	sample *out = (sample*)outputBuffer;
    const sample *in = (const sample*)inputBuffer;
    chainParams ch;

    // Prevent unused variable warnings.
    (void) timeInfo;
    (void) statusFlags;

    if(inputBuffer == NULL)
    {
    	for(unsigned int i=0; i<framesPerBuffer; i++)
        {
        	*out++ = 0;  // left - silent
            *out++ = 0;  // right - silent
        }
        gNumNoInputs += 1;
    } else {
    	for(unsigned int i=0; i<framesPerBuffer; i++)
        {
        	// Input
            frame inFrame = { .len=data->inputsNum };
            for(int j=0; j<inFrame.len; j++) inFrame.sa[j]=*in++;
            ch.fr = inFrame;

            frame outFrame = { .len=2, .sa[0]=0.0, .sa[1]=0.0 };
            // Select chain:
            switch(data->chainNum)
            {
            case 0:
            	outFrame = chain0(&ch);
            	break;
            case 1:
            	outFrame = chain1(&ch);
            	break;
            case 2:
                outFrame = chain2(&ch);
                break;
            case 3:
                outFrame = chain3(&ch);
                break;
            case 4:
                outFrame = chain4(&ch);
                break;
            case 5:
                outFrame = chain5(&ch);
                break;
            case 6:
                outFrame = chain6(&ch);
                break;
            case 7:
                outFrame = chain7(&ch);
                break;
            case 8:
                outFrame = chain8(&ch);
                break;
            case 9:
                outFrame = chain9(&ch);
                break;
            default:
            	outFrame = chain0(&ch);
            	break;
            }


            // Convert mono->stereo when output is stereo and signal behind chain is mono
            if(data->outputsNum > outFrame.len)
            {
            	outFrame.sa[1] = outFrame.sa[0];
            	outFrame.len=2;
            }

            // Output
            for(int j=0; j<outFrame.len; j++) *out++=outFrame.sa[j];
        }
    }

    return paContinue;
}

int main(int argc, char *argv[])
{
    PaStreamParameters inputParameters, outputParameters;
    PaStream *stream;
    PaError err;

    if(argc==1)
	{
    	printf("No effect chain selected\n");
    	return -1;
	}

    // Load setup
    cbParams callbackParams = { .chainNum = *argv[1]-'0' };

    // PortAudio init
    err = Pa_Initialize();
    if(err != paNoError) Error(err);

    // Print audio devices
    /*printf("Devices:\n");
	for(int i=0; i<Pa_GetDeviceCount(); i++)
	{
		const PaDeviceInfo* info;
		info = Pa_GetDeviceInfo(i);
		printf("%d: %s\n",i,info->name);
	}*/

    int inputDev = Pa_GetDefaultInputDevice();
    int outputDev = Pa_GetDefaultOutputDevice();

    for(int i=0; i<Pa_GetDeviceCount(); i++)
    {
		const PaDeviceInfo* info;
		info = Pa_GetDeviceInfo(i);
		if(argc>2 && !strcmp(info->name,argv[2])) inputDev = i;
		if(argc>3 && !strcmp(info->name,argv[3])) outputDev = i;
    }

    inputParameters.device = inputDev;
    if (inputParameters.device == paNoDevice) {
      fprintf(stderr,"Error: No default input device.\n");
      Error(err);
    }
    inputParameters.channelCount = 2;
    inputParameters.sampleFormat = PA_SAMPLE_TYPE;
    inputParameters.suggestedLatency = Pa_GetDeviceInfo(inputParameters.device)->defaultLowInputLatency;
    inputParameters.hostApiSpecificStreamInfo = NULL;

    outputParameters.device = outputDev;
    if (outputParameters.device == paNoDevice) {
      fprintf(stderr,"Error: No default output device.\n");
      Error(err);
    }
    outputParameters.channelCount = 2;
    outputParameters.sampleFormat = PA_SAMPLE_TYPE;
    outputParameters.suggestedLatency = Pa_GetDeviceInfo(outputParameters.device)->defaultLowOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;

    callbackParams.inputsNum = inputParameters.channelCount;
    callbackParams.outputsNum = outputParameters.channelCount;

    err = Pa_OpenStream(
              &stream,
              &inputParameters,
              &outputParameters,
              SAMPLE_RATE,
              FRAMES_PER_BUFFER,
              1,
              bz_callback,
              &callbackParams);
    if(err != paNoError) Error(err);

    err = Pa_StartStream(stream);
    if(err != paNoError) Error(err);

    printf("Hit ENTER to stop program.\n");
    getchar();
    err = Pa_CloseStream(stream);
    if(err != paNoError) Error(err);

    printf("Finished. gNumNoInputs = %d\n", gNumNoInputs);
    Pa_Terminate();
    return 0;
}
